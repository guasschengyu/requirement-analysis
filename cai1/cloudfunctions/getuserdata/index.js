// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  return cloud.database().collection("user").where({
    "_openid":event.openid
  }).get({
    success: function (res) {
      console.log("信息读取成功",res)
    },
    fail(res){
      console.log("获取个人信息失败",res)
    }
  })
}